const AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = 'HelloTable';
const uuidv1 = require('uuid/v1');
exports.lambdaHandler = (event, context, callback) => {
    console.log('TCL: exports.lambdaHandler -> event', event);
    // Procesamiento correcto con la cadena "Hello World!"
    saveItem(event, callback);
};

function saveItem(event, callback) {
    const item = event;
    
    item.ItemId = uuidv1();
    const params = {
		TableName: TABLE_NAME,
		Item: item
    };
    console.log('TCL: saveItem -> params', params);
    console.log('TCL: saveItem -> item', item);
    
    dynamodb.put(params, function(err, data) {
        if (err) {
            console.log(err);
            sendResponse(400, 'Something wrong',callback);
        } else {
            console.log(data);
            sendResponse(200, item, callback);
        }
    });
}

function sendResponse(statusCode, message, callback) {
    const response = {
        statusCode: statusCode,
        body: JSON.stringify(message)
    };
    callback(null, response);
}
